import org.apache.commons.math3.util.FastMath;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.stream.IntStream;

/**
 * Created by GomaTa on 14.05.2016.
 */
public class GChart {
    private int imageWidth = -1;
    private int imageHeight = 200;
    private int margin = 50;
    private Graphics2D ig2;
    private double gap = 2.0;
    private int axisThickness = 1;
    private int graphThickness = 1;
    private double minValue;
    private double maxValue;
    private double diffMinMax;
    private int chartHeight;
    private int chartWidth;
    private int maxNegativHeight;
    private int maxPositivHeight;
    private int xZero;
    private int yAxisZero;
    private int chartYZero;
    private int chartXZero;
    private boolean captionXAxis = false;
    private boolean captionYAxis = false;

    public void setMargin(int margin) {
        this.margin = margin;
    }

    public void setGap(double gap) {
        this.gap = gap;
    }

    public void setAxisThickness(int axisThickness) {
        this.axisThickness = axisThickness;
    }

    public void setGraphThickness(int graphThickness) {
        this.graphThickness = graphThickness;
    }

    public void setImageHeight(int imageHeight) {
        this.imageHeight = imageHeight;
    }

    public void setImageWidth(int imageWidth) {
        this.imageWidth = imageWidth;
    }

    public void setCaptionXAxis(boolean captionXAxis) {
        this.captionXAxis = captionXAxis;
    }

    public void setCaptionYAxis(boolean captionYAxis) {
        this.captionYAxis = captionYAxis;
    }

    public void draw(String filepath, double[] samples) {
        double[] xAxis = IntStream.range(0, samples.length).mapToDouble(k -> k).toArray();
        draw(filepath, samples, xAxis);
    }

    public void draw(String filepath, double[] values, double[] xAxis) {
        try {
            //caluclate image width
            imageWidth = (imageWidth == -1)? (int) FastMath.ceil((gap) * values.length) + (2 * margin): imageWidth;
            BufferedImage bi = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            ig2 = bi.createGraphics();
            //caluclate chart yZero and xZero
            chartYZero = margin;
            chartXZero = margin;
            //calculate min and max for y axis and its difference
            minValue = getMinValue(values);
            maxValue = getMaxValue(values);
            diffMinMax = FastMath.abs(minValue-maxValue);
            //calculate chart height and width
            chartHeight = imageHeight - (2 * margin);
            chartWidth = imageWidth - (2 * margin);
            //calculate x and y zero
            yAxisZero = (int) (chartHeight * (maxValue / Double.valueOf(FastMath.abs(minValue - maxValue)))) + margin;
            //calculate max positive and negative pixel
            maxPositivHeight = (int) FastMath.round(chartHeight * (maxValue / Double.valueOf(FastMath.abs(minValue - maxValue))));
            maxNegativHeight = (int) FastMath.round(chartHeight * (FastMath.abs(minValue) / Double.valueOf(FastMath.abs(minValue - maxValue))));
            //draw background
            ig2.setColor(Color.WHITE);
            ig2.fillRect(0, 0, imageWidth, imageHeight);
            //draw y and x yAxis
            drawYAxis(Color.BLACK);
            drawXAxis(xAxis, Color.BLACK);
            //draw samples
            drawValues(values, Color.BLUE);
            //save image
            ImageIO.write(bi, "PNG", new File(filepath));
        } catch (IOException ie) {
            ie.printStackTrace();
        }
    }

    public double getMaxValue(double[] values) {
        double max = Double.MIN_VALUE;
        for (double v : values) {
            if (v > max) {
                max = v;
            }
        }
        return round(max);
    }

    public double getMinValue(double[] values) {
        double min = Double.MAX_VALUE;
        for (double v : values) {
            if (v < min) {
                min = v;
            }
        }
        return round(min);
    }

    public double round(double value){
        if(value > 1.0){
            return Double.valueOf((int) FastMath.ceil(value));
        }else if(value < -1.0){
            return Double.valueOf((int) FastMath.floor(value));
        }else if(value == 0.0){
            return 0.0;
        }else{
            return roundSmall(value);
        }
    }

    private double roundSmall(double value) {
        int numDec = 0;
        double tmpValue = FastMath.abs(value);
        while (tmpValue < 1.0){
            numDec++;
            tmpValue = tmpValue * 10.0;
        }
        tmpValue = (value < 0.0)? -1.0: 1.0;
        for(int i = 1; i < numDec; i++){
            tmpValue = tmpValue / 10.0;
        }
        return tmpValue;
    }

    private void drawXAxis(double[] xAxis, Color c) {
        int x, y, width, height;
        //draw x axis
        x = margin;
        y = yAxisZero;
        width = imageWidth - (2 * margin);
        height = axisThickness;
        ig2.setColor(c);
        ig2.fillRect(x, y, width, height);
        //draw marks
        y = y - axisThickness;
        height = 3 * axisThickness;
        width = axisThickness;
        for (int i = 0; i < xAxis.length; i++) {
            x = margin + (int) ((i + 1) * gap);
            if(x <= (imageWidth-margin)) {
                ig2.fillRect(x, y, width, height);
                if (captionXAxis) {
                    String caption = String.format("%.2f", xAxis[i]);
                    ig2.drawString(caption, x - 15, y + 20);
                }
            }
        }
    }

    private void drawYAxis(Color c) {
        int numValues = (int) FastMath.ceil(diffMinMax*5);
        double stepYAxis = diffMinMax / Double.valueOf(numValues);
        double gapYAxis = chartHeight / Double.valueOf(numValues);
        int x, y, height, width;
        ig2.setColor(c);
        x = margin;
        y = margin;
        height = imageHeight - (2 * margin);
        width = axisThickness;
        ig2.fillRect(x, y, width, height);
        for (int i = 0; i <= numValues; i++) {
            x = margin - axisThickness;
            y = margin + (int) (i * gapYAxis);
            width = 3 * axisThickness;
            height = axisThickness;
            ig2.fillRect(x, y, width, height);
            if (captionYAxis) {
                String caption = String.format("%.2f", maxValue - (i * stepYAxis));
                ig2.drawString(caption, x - 30, y + 5);
            }
        }
    }

    private void drawValues(double[] values, Color c) {
        ig2.setColor(c);
        int step = 1;
        if((values.length * gap) > chartWidth){
            step = (int) FastMath.ceil(values.length / Double.valueOf(chartWidth / gap));
        }
        int x, y, height;
        int width = graphThickness;
        for (int i = 0, j = 0; j < values.length; i++, j+= step) {
            x = (int) (margin + (i + 1) * gap) - (graphThickness / 2);
            if(x <= (imageWidth-margin)) {
                if (values[j] < 0) {//negative values
                    y = yAxisZero;
                    height = (int) (maxNegativHeight * values[j] / minValue);
                    ig2.fillRect(x, y, width, height);
                } else {//positiv values
                    height = (int) (maxPositivHeight * values[j] / maxValue);
                    y = yAxisZero - height;
                    ig2.fillRect(x, y, width, height);
                }
            }
        }
    }
}
